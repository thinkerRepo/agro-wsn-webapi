package mk.finki.ekoinfo;

import mk.finki.ekoinfo.filters.AuthenticationFilter;
import mk.finki.ekoinfo.filters.CORSResponseFilter;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class App extends ResourceConfig {

    public App(){
        packages("mk.ukim.finki.misw.busline");
        register(MoxyJsonFeature.class);
        register(CORSResponseFilter.class);
        register(AuthenticationFilter.class);
    }
}
