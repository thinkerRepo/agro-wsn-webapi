package mk.finki.ekoinfo;

import mk.finki.ekoinfo.repositories.utils.HibernateFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class StartupContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        HibernateFactory.init();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
