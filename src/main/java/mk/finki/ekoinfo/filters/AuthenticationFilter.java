package mk.finki.ekoinfo.filters;

import mk.finki.ekoinfo.exceptions.UnauthorizedException;
import mk.finki.ekoinfo.filters.anotations.Secured;
import mk.finki.ekoinfo.services.models.UserServiceModel;
import mk.finki.ekoinfo.utils.JWTUtils;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw new UnauthorizedException("Authorization header must be provided");
        }

        String token = authorizationHeader.substring("Bearer".length()).trim();
        UserServiceModel user = JWTUtils.verifyToken(token);
        requestContext.setProperty("authUser", user);
    }

}