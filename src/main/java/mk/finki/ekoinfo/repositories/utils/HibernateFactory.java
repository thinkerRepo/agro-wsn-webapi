package mk.finki.ekoinfo.repositories.utils;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateFactory {

    private static SessionFactory sessionFactory = null;

    private HibernateFactory() {
    }


    public static Session createSession()
    {
        return sessionFactory.openSession();
    }


    public static void init()
    {
        if (sessionFactory == null) {
            synchronized (HibernateFactory.class) {
                if (sessionFactory == null) {
                    sessionFactory = new Configuration().configure().buildSessionFactory();
                }
            }
        }
    }



}
