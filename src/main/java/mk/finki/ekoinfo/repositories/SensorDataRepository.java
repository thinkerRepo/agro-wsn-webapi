package mk.finki.ekoinfo.repositories;


import mk.finki.ekoinfo.exceptions.DataNotFoundException;
import mk.finki.ekoinfo.exceptions.TechnicalException;
import mk.finki.ekoinfo.mappers.dbToService.SensorDataDTSMapper;
import mk.finki.ekoinfo.mappers.serviceToDb.SensorDataSTDMapper;
import mk.finki.ekoinfo.repositories.models.GetChartTTNDataDbModel;
import mk.finki.ekoinfo.repositories.models.GetWineQualityDataDbModel;
import mk.finki.ekoinfo.repositories.models.SensorDataDbModel;
import mk.finki.ekoinfo.repositories.models.TTNSensorDataDbModel;
import mk.finki.ekoinfo.repositories.utils.HibernateFactory;
import mk.finki.ekoinfo.services.models.*;
import mk.finki.ekoinfo.utils.DBUtils;
import mk.finki.ekoinfo.utils.DateUtils;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class SensorDataRepository {

    public List<SensorDataServiceModel> getAll(){
        List<SensorDataServiceModel> sensors = new ArrayList<>();

        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            CriteriaQuery<SensorDataDbModel> cq = session.getCriteriaBuilder().createQuery(SensorDataDbModel.class);
            cq.from(SensorDataDbModel.class);
            List<SensorDataDbModel> sensorsDb = session.createQuery(cq).getResultList();

            session.getTransaction().commit();
            return SensorDataDTSMapper.map(sensorsDb);

        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public SensorDataServiceModel get(int id) {
        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            SensorDataDbModel sensor = session.get(SensorDataDbModel.class,id);
            if(sensor == null) throw new DataNotFoundException("Бараниот сензор не е пронајден!");

            session.getTransaction().commit();
            return SensorDataDTSMapper.map(sensor);

        } catch (DataNotFoundException ex) {
            throw new DataNotFoundException(ex.getMessage());
        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public SensorDataServiceModel add(SensorDataServiceModel sensor){

        SensorDataDbModel sensorDb = SensorDataSTDMapper.map(sensor);

        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            int id = (int)session.save(sensorDb);
            sensorDb = session.get(SensorDataDbModel.class,id);

            session.getTransaction().commit();
            return SensorDataDTSMapper.map(sensorDb);
        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public SensorDataServiceModel edit(int id, SensorDataServiceModel sensor){

        return sensor;
    }

    public boolean remove(int id){
        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            SensorDataDbModel sensor = session.get(SensorDataDbModel.class,id);
            if(sensor == null) throw new DataNotFoundException("Бараниот податок за сензорот не е пронајден!");
            session.delete(sensor);

            session.getTransaction().commit();
            return true;

        } catch (DataNotFoundException ex) {
            throw new DataNotFoundException(ex.getMessage());
        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public TTNSensorDataServiceModel addTTNData(TTNSensorDataServiceModel ttnData) {
        TTNSensorDataDbModel sensorDb = SensorDataSTDMapper.map(ttnData);

        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            sensorDb.setId(UUID.randomUUID());
            UUID id = (UUID)session.save(sensorDb);
            sensorDb = session.get(TTNSensorDataDbModel.class,id);

            session.getTransaction().commit();
            return SensorDataDTSMapper.map(sensorDb);
        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public List<TTNSensorDataServiceModel> getAllTTNData(){

        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<TTNSensorDataDbModel> cq = builder.createQuery(TTNSensorDataDbModel.class);
            Root<TTNSensorDataDbModel> ttnDb = cq.from(TTNSensorDataDbModel.class);
            cq.orderBy(builder.desc(ttnDb.get("date")));
            List<TTNSensorDataDbModel> dataDb = session.createQuery(cq).setFirstResult(0).setMaxResults(50).getResultList();

            session.getTransaction().commit();
            return SensorDataDTSMapper.mapTTNData(dataDb);

        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }
    }


    public List<TTNSensorDataServiceModel> getAllTTNDataFiltered(GetTTNDataFilteredServiceModel serviceModel){

        Date dateFrom = DateUtils.setTime(serviceModel.getDateFrom(), 0, 0, 0, 0);
        Date dateTo = DateUtils.setTime(serviceModel.getDateTo(), 23, 59, 59, 999);

        /*String startDate = "2017-07-01";
        String endDate = "2017-07-10";
        Date dateFrom = new Date(), dateTo = new Date();

        try {
            dateFrom =new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
            dateTo =new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
        } catch(Exception ex) {

        }*/


        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            String sql = "SELECT id, altitude, battery_status, date, device_id, humidity, latitude, leaf_wetness, longitude, soil_moisture, temperature FROM ttn_sensor_data WHERE date >= :dateFrom AND date <= :dateTo ORDER BY date";
            Query query = session.createNativeQuery(sql, TTNSensorDataDbModel.class);
            query.setParameter("dateFrom", dateFrom);
            query.setParameter("dateTo", dateTo);

            @SuppressWarnings("unchecked")
            List<TTNSensorDataDbModel> result = (List<TTNSensorDataDbModel>) query.getResultList();
            session.getTransaction().commit();

            return SensorDataDTSMapper.mapTTNData(result);

        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }

    }



    public List<GetChartTTNDataServiceModel> getChartTTNData(){
        String sql = "SELECT EXTRACT(hour from date)::integer as hour, AVG(temperature)::integer as temperature, AVG(humidity)::integer as humidity, " +
                "AVG(leaf_wetness)::integer as leaf_wetness, " +
                "AVG(soil_moisture)::integer as soil_moisture " +
                "FROM ttn_sensor_data " +
                "WHERE date >= '2018-04-04 00:00:00.000' and date <= '2018-04-04 23:59:59.000' " +
                "GROUP BY hour " +
                "ORDER BY hour";

        try(Connection conn = DBUtils.getConnection()){
            conn.setAutoCommit(false);

            try (
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery(sql);
            )
            {

                List<GetChartTTNDataDbModel> result = new ArrayList<>();

                while(rs.next()){
                    GetChartTTNDataDbModel entity = new GetChartTTNDataDbModel();

                    entity.setHour(rs.getInt("hour"));
                    entity.setTemperature(rs.getInt("temperature"));
                    entity.setHumidity(rs.getInt("humidity"));
                    entity.setSoilMoisture(rs.getInt("soil_moisture"));
                    entity.setLeafWetness(rs.getInt("leaf_wetness"));

                    result.add(entity);
                }

                conn.commit();
                conn.setAutoCommit(true);
                return SensorDataDTSMapper.mapGetChartTTNData(result);
            } catch (SQLException ex) {
                conn.rollback();
                conn.setAutoCommit(true);
                throw new TechnicalException(ex.getMessage());
            }
        } catch (Exception ex){
            throw new TechnicalException(ex.getMessage());
        }

    }


    public List<GetWineQualityDataServiceModel> getWineQualityModelData(){
        String sql = "select date::date::character varying as iso_date, avg(temperature)::integer as temperature " +
                "from ttn_sensor_data " +
                "where date >= '2017-04-01' and date < '2017-10-01' " +
                "group by iso_date " +
                "order by iso_date";

        try(Connection conn = DBUtils.getConnection()){
            conn.setAutoCommit(false);

            try (
                    Statement stmt = conn.createStatement();
                    ResultSet rs = stmt.executeQuery(sql);
            )
            {

                List<GetWineQualityDataDbModel> result = new ArrayList<>();

                while(rs.next()){
                    GetWineQualityDataDbModel entity = new GetWineQualityDataDbModel();

                    entity.setDate(rs.getString("iso_date"));
                    entity.setTemperature(rs.getInt("temperature"));

                    result.add(entity);
                }

                conn.commit();
                conn.setAutoCommit(true);
                return SensorDataDTSMapper.mapGetWineQualityData(result);
            } catch (SQLException ex) {
                conn.rollback();
                conn.setAutoCommit(true);
                throw new TechnicalException(ex.getMessage());
            }
        } catch (Exception ex){
            throw new TechnicalException(ex.getMessage());
        }
    }



}
