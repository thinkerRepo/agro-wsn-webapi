package mk.finki.ekoinfo.repositories;

import mk.finki.ekoinfo.exceptions.models.user.UserDoesNotExist;
import mk.finki.ekoinfo.mappers.dbToService.UserDTSMapper;
import mk.finki.ekoinfo.repositories.models.UserDbModel;
import mk.finki.ekoinfo.services.models.UserServiceModel;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserRepository {

    //private MongoCollection<UserDbModel> collection = MongoManager.getCollection("users", UserDbModel.class);

    public List<UserServiceModel> getAll(){
        /*List<UserServiceModel> users = new ArrayList<>();
        MongoCursor<UserDbModel> cursor = collection.find().iterator();

        try {
            while (cursor.hasNext()) {
                users.add(UserDTSMapper.map(cursor.next()));
            }
        } finally {
            cursor.close();
        }

        return users;*/

        return null;
    }

    public UserServiceModel get(UUID id){
        /*UserDbModel user = collection.find(eq("_id", id)).first();
        if(user == null) throw new UserDoesNotExist();
        return UserDTSMapper.map(user);*/
        return null;
    }

    public UserServiceModel add(UserServiceModel user){
        /*user.setId(UUID.randomUUID());
        collection.insertOne(UserSTDMapper.map(user));
        return user;*/

        return null;
    }

    public UserServiceModel edit(UUID id, UserServiceModel user){
        /*user.setId(id);
        UserDbModel editedUser = collection.findOneAndReplace(eq("_id", id), UserSTDMapper.map(user));
        if(editedUser == null) throw new UserDoesNotExist();
        return user;*/
        return null;
    }

    public boolean remove(UUID id){
        /*DeleteResult deleteResult = collection.deleteOne(eq("_id", id));
        if (deleteResult.getDeletedCount() == 0) throw new UserDoesNotExist();
        return true;*/
        return false;
    }


    public UserServiceModel getUserByUsername(String username){
        /*UserDbModel user = collection.find(eq("username", username)).first();
        if(user == null) throw new UserDoesNotExist();
        return UserDTSMapper.map(user);*/
        return null;
    }

}
