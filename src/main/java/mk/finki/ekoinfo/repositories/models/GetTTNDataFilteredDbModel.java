package mk.finki.ekoinfo.repositories.models;

import java.util.Date;

public class GetTTNDataFilteredDbModel {
    private Date dateFrom;
    private Date dateTo;



    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }
}
