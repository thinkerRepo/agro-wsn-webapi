package mk.finki.ekoinfo.repositories.models;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class GetChartTTNDataDbModel {

    @Column(name = "hour")
    private int hour;

    @Column(name = "temperature")
    private int temperature;

    @Column(name = "humidity")
    private int humidity;

    @Column(name = "soilMoisture")
    private int soilMoisture;

    @Column(name = "leafWetness")
    private int leafWetness;


    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getSoilMoisture() {
        return soilMoisture;
    }

    public void setSoilMoisture(int soilMoisture) {
        this.soilMoisture = soilMoisture;
    }

    public int getLeafWetness() {
        return leafWetness;
    }

    public void setLeafWetness(int leafWetness) {
        this.leafWetness = leafWetness;
    }

}
