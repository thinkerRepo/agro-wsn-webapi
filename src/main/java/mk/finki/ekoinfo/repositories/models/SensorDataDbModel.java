package mk.finki.ekoinfo.repositories.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "sensor_data")
public class SensorDataDbModel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "sensor_id")
    private int sensorId;
    @Column(name = "date")
    private Date date;

    @Column(name = "air_temperature")
    private float airTemperature;
    @Column(name = "air_humidity")
    private float airHumidity;
    @Column(name = "soil_temperature")
    private float soilTemperature;
    @Column(name = "soil_humidity")
    private float soilHumidity;
    @Column(name = "soil_ph")
    private float soilPH;
    @Column(name = "leaf_wetness")
    private float leafWetness;
    @Column(name = "luminosity")
    private float luminosity;


    public SensorDataDbModel() {

    }

    public SensorDataDbModel(int id) {
        this.id = id;
    }

    public SensorDataDbModel(int id, int sensorId, Date date, float airTemperature, float airHumidity, float soilTemperature, float soilHumidity, float soilPH, float leafWetness, float luminosity) {
        this.id = id;
        this.sensorId = sensorId;
        this.date = date;
        this.airTemperature = airTemperature;
        this.airHumidity = airHumidity;
        this.soilTemperature = soilTemperature;
        this.soilHumidity = soilHumidity;
        this.soilPH = soilPH;
        this.leafWetness = leafWetness;
        this.luminosity = luminosity;
    }







    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getAirTemperature() {
        return airTemperature;
    }

    public void setAirTemperature(float airTemperature) {
        this.airTemperature = airTemperature;
    }

    public float getAirHumidity() {
        return airHumidity;
    }

    public void setAirHumidity(float airHumidity) {
        this.airHumidity = airHumidity;
    }

    public float getSoilTemperature() {
        return soilTemperature;
    }

    public void setSoilTemperature(float soilTemperature) {
        this.soilTemperature = soilTemperature;
    }

    public float getSoilHumidity() {
        return soilHumidity;
    }

    public void setSoilHumidity(float soilHumidity) {
        this.soilHumidity = soilHumidity;
    }

    public float getSoilPH() {
        return soilPH;
    }

    public void setSoilPH(float soilPH) {
        this.soilPH = soilPH;
    }

    public float getLeafWetness() {
        return leafWetness;
    }

    public void setLeafWetness(float leafWetness) {
        this.leafWetness = leafWetness;
    }

    public float getLuminosity() {
        return luminosity;
    }

    public void setLuminosity(float luminosity) {
        this.luminosity = luminosity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
