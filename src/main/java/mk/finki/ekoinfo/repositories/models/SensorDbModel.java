package mk.finki.ekoinfo.repositories.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sensor")
public class SensorDbModel {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="sensor_id_seq")
    @SequenceGenerator(name="sensor_id_seq", sequenceName="sensor_id_seq", allocationSize=1)
    private int id;

    @Column(name = "sifra")
    private String sifra;

    @Column(name = "lat")
    private float lat;

    @Column(name = "lon")
    private float lon;



    public SensorDbModel() {

    }

    public SensorDbModel(int id) {
        this.id = id;
    }

    public SensorDbModel(int id, String sifra, int lat, int lon) {
        this.id = id;
        this.sifra = sifra;
        this.lat = lat;
        this.lon = lon;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }


    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }
}
