package mk.finki.ekoinfo.repositories.models;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "ttn_sensor_data")
public class TTNSensorDataDbModel {

    @Id
    @Type(type="pg-uuid")
    @Column(name = "id")
    private UUID id;

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "date")
    private Date date;

    @Column(name = "latitude")
    private float latitude;

    @Column(name = "longitude")
    private float longitude;

    @Column(name = "altitude")
    private float altitude;

    @Column(name = "battery_status")
    private boolean batteryStatus;

    @Column(name = "humidity")
    private int humidity;

    @Column(name = "leaf_wetness")
    private int leafWetness;

    @Column(name = "soil_moisture")
    private int soilMoisture;

    @Column(name = "temperature")
    private int temperature;





    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }

    public boolean isBatteryStatus() {
        return batteryStatus;
    }

    public void setBatteryStatus(boolean batteryStatus) {
        this.batteryStatus = batteryStatus;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getLeafWetness() {
        return leafWetness;
    }

    public void setLeafWetness(int leafWetness) {
        this.leafWetness = leafWetness;
    }

    public int getSoilMoisture() {
        return soilMoisture;
    }

    public void setSoilMoisture(int soilMoisture) {
        this.soilMoisture = soilMoisture;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }
}


