package mk.finki.ekoinfo.repositories.models;

public class GetWineQualityDataDbModel {

    private String date;
    private int temperature;



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

}
