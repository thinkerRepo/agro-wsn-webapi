package mk.finki.ekoinfo.repositories;


import mk.finki.ekoinfo.exceptions.DataNotFoundException;
import mk.finki.ekoinfo.exceptions.TechnicalException;
import mk.finki.ekoinfo.mappers.dbToService.SensorDTSMapper;
import mk.finki.ekoinfo.mappers.serviceToDb.SensorSTDMapper;
import mk.finki.ekoinfo.repositories.utils.HibernateFactory;
import mk.finki.ekoinfo.services.models.SensorServiceModel;
import org.hibernate.Session;

import mk.finki.ekoinfo.repositories.models.SensorDbModel;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.List;


public class SensorRepository {

    public List<SensorServiceModel> getAll(){
        List<SensorServiceModel> sensors = new ArrayList<>();

        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            CriteriaQuery<SensorDbModel> cq = session.getCriteriaBuilder().createQuery(SensorDbModel.class);
            cq.from(SensorDbModel.class);
            List<SensorDbModel> sensorsDb = session.createQuery(cq).getResultList();

            session.getTransaction().commit();
            return SensorDTSMapper.map(sensorsDb);

        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public SensorServiceModel get(int id) {
        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            SensorDbModel sensor = session.get(SensorDbModel.class,id);
            if(sensor == null) throw new DataNotFoundException("Бараниот сензор не е пронајден!");

            session.getTransaction().commit();
            return SensorDTSMapper.map(sensor);

        } catch (DataNotFoundException ex) {
            throw new DataNotFoundException(ex.getMessage());
        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public SensorServiceModel add(SensorServiceModel sensor){

        SensorDbModel sensorDb = SensorSTDMapper.map(sensor);

        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            int id = (int)session.save(sensorDb);
            sensorDb = session.get(SensorDbModel.class,id);

            session.getTransaction().commit();
            return SensorDTSMapper.map(sensorDb);
        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }
    }

    public SensorServiceModel edit(int id, SensorServiceModel sensor){

        return sensor;
    }

    public boolean remove(int id){
        Session session = HibernateFactory.createSession();
        try {
            session.beginTransaction();

            SensorDbModel sensor = session.get(SensorDbModel.class,id);
            if(sensor == null) throw new DataNotFoundException("Бараниот сензор не е пронајден!");
            session.delete(sensor);

            session.getTransaction().commit();
            return true;

        } catch (DataNotFoundException ex) {
            throw new DataNotFoundException(ex.getMessage());
        } catch (Exception ex) {
            session.getTransaction().rollback();
            throw new TechnicalException(ex.getMessage());
        } finally {
            session.close();
        }
    }


}
