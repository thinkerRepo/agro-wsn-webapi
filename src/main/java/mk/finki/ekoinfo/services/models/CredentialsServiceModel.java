package mk.finki.ekoinfo.services.models;

public class CredentialsServiceModel {

    private String username;
    private String password;

    public CredentialsServiceModel() {}

    public CredentialsServiceModel(String username, String password) {
        this.username = username;
        this.password = password;
    }


    /// getters & setters /////////

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
