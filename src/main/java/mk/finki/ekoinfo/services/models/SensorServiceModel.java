package mk.finki.ekoinfo.services.models;


public class SensorServiceModel {

    private int id;
    private String sifra;
    private float lat;
    private float lon;


    public SensorServiceModel() {

    }

    public SensorServiceModel(int id, String sifra, int lat, int lon) {
        this.id = id;
        this.sifra = sifra;
        this.lat = lat;
        this.lon = lon;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSifra() {
        return sifra;
    }

    public void setSifra(String sifra) {
        this.sifra = sifra;
    }


    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }
}
