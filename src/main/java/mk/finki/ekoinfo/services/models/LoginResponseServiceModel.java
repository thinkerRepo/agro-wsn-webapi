package mk.finki.ekoinfo.services.models;

public class LoginResponseServiceModel {

    private String token;
    private UserServiceModel user;

    public LoginResponseServiceModel() {}

    public LoginResponseServiceModel(String token, UserServiceModel user) {
        this.token = token;
        this.user = user;
    }


    /// getters & setters /////////

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserServiceModel getUser() {
        return user;
    }

    public void setUser(UserServiceModel user) {
        this.user = user;
    }
}
