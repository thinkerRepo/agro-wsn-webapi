package mk.finki.ekoinfo.services.models;

import java.util.UUID;

public class UserServiceModel {

    private UUID id;
    private String username;
    private String password;

    public UserServiceModel() {}

    public UserServiceModel(UUID id, String username, String password){
        this.id = id;
        this.username = username;
        this.password = password;
    }


    /// getters & setters /////////

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
