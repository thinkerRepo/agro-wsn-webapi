package mk.finki.ekoinfo.services.models;

public class GetWineQualityDataServiceModel {

    private String date;
    private int temperature;



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

}
