package mk.finki.ekoinfo.services.models;

import java.util.Date;
import java.util.UUID;

/**
 * Created by srbo on 13-Mar-17.
 */
public class TTNSensorDataServiceModel {

    private UUID id;
    private Date date;
    private String deviceId;
    private float latitude;
    private float longitude;
    private float altitude;
    private boolean batteryStatus;
    private int humidity;
    private int leafWetness;
    private int soilMoisture;
    private int temperature;


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }

    public boolean isBatteryStatus() {
        return batteryStatus;
    }

    public void setBatteryStatus(boolean batteryStatus) {
        this.batteryStatus = batteryStatus;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getLeafWetness() {
        return leafWetness;
    }

    public void setLeafWetness(int leafWetness) {
        this.leafWetness = leafWetness;
    }

    public int getSoilMoisture() {
        return soilMoisture;
    }

    public void setSoilMoisture(int soilMoisture) {
        this.soilMoisture = soilMoisture;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }
}
