package mk.finki.ekoinfo.services.models;


import java.util.Date;

public class SensorDataServiceModel {

    private int id;
    private int sensorId;
    private Date date;

    private float airTemperature;
    private float airHumidity;
    private float soilTemperature;
    private float soilHumidity;
    private float soilPH;
    private float leafWetness;
    private float luminosity;


    public SensorDataServiceModel() {

    }

    public SensorDataServiceModel(int id, int sensorId, Date date, float airTemperature, float airHumidity, float soilTemperature, float soilHumidity, float soilPH, float leafWeatness, float luminosity) {
        this.id = id;
        this.sensorId = sensorId;
        this.date = date;
        this.airTemperature = airTemperature;
        this.airHumidity = airHumidity;
        this.soilTemperature = soilTemperature;
        this.soilHumidity = soilHumidity;
        this.soilPH = soilPH;
        this.leafWetness = leafWeatness;
        this.luminosity = luminosity;
    }

    public int getSensorId() {
        return sensorId;
    }

    public void setSensorId(int sensorId) {
        this.sensorId = sensorId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getAirTemperature() {
        return airTemperature;
    }

    public void setAirTemperature(float airTemperature) {
        this.airTemperature = airTemperature;
    }

    public float getAirHumidity() {
        return airHumidity;
    }

    public void setAirHumidity(float airHumidity) {
        this.airHumidity = airHumidity;
    }

    public float getSoilTemperature() {
        return soilTemperature;
    }

    public void setSoilTemperature(float soilTemperature) {
        this.soilTemperature = soilTemperature;
    }

    public float getSoilHumidity() {
        return soilHumidity;
    }

    public void setSoilHumidity(float soilHumidity) {
        this.soilHumidity = soilHumidity;
    }

    public float getSoilPH() {
        return soilPH;
    }

    public void setSoilPH(float soilPH) {
        this.soilPH = soilPH;
    }

    public float getLeafWetness() {
        return leafWetness;
    }

    public void setLeafWetness(float leafWetness) {
        this.leafWetness = leafWetness;
    }

    public float getLuminosity() {
        return luminosity;
    }

    public void setLuminosity(float luminosity) {
        this.luminosity = luminosity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
