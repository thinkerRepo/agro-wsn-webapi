package mk.finki.ekoinfo.services;


import mk.finki.ekoinfo.repositories.SensorDataRepository;
import mk.finki.ekoinfo.services.models.*;
import mk.finki.ekoinfo.utils.MapRange;

import javax.ws.rs.GET;
import java.util.List;

public class SensorDataService {


    private SensorDataRepository repository;

    public SensorDataService(){
        this.repository = new SensorDataRepository();
    }


    /// service methods /////////

    public List<SensorDataServiceModel> getAll(){
        return repository.getAll();
    }

    public SensorDataServiceModel get(int id){
        return repository.get(id);
    }

    public SensorDataServiceModel add(SensorDataServiceModel sensor){
        SensorDataServiceModel sensorServiceModel = repository.add(sensor);
        return sensorServiceModel;
    }

    public SensorDataServiceModel edit(int id, SensorDataServiceModel sensor){
        SensorDataServiceModel sensorServiceModel = repository.edit(id, sensor);
        return sensorServiceModel;
    }

    public boolean remove(int id) {
        boolean sensorRemoved =  repository.remove(id);
        SensorDataServiceModel sensorServiceModel = new SensorDataServiceModel();
        sensorServiceModel.setId(id);
        return sensorRemoved;
    }

    public TTNSensorDataServiceModel addTTNData(TTNSensorDataServiceModel ttnData) {
        return repository.addTTNData(ttnData);
    }

    public List<TTNSensorDataServiceModel> getTTNData() {
        return repository.getAllTTNData();
    }

    public List<TTNSensorDataServiceModel> getAllTTNDataFiltered(GetTTNDataFilteredServiceModel serviceModel) {
        return repository.getAllTTNDataFiltered(serviceModel);
    }

    public List<GetChartTTNDataServiceModel> getChartTTNData(){
        List<GetChartTTNDataServiceModel> resultData = repository.getChartTTNData();
        resultData.forEach(x -> {
            x.setSoilMoisture(MapRange.map(x.getSoilMoisture(), 1023, 0, 0, 100));
            x.setLeafWetness(MapRange.map(x.getLeafWetness(), 1023, 0, 0, 100));
        });
        return resultData;
    }

    public List<GetWineQualityDataServiceModel> getWineQualityModelData(){
        return repository.getWineQualityModelData();
    }

}
