package mk.finki.ekoinfo.services;

import mk.finki.ekoinfo.exceptions.UnauthorizedException;
import mk.finki.ekoinfo.exceptions.models.user.UserDoesNotExist;
import mk.finki.ekoinfo.repositories.UserRepository;
import mk.finki.ekoinfo.services.models.CredentialsServiceModel;
import mk.finki.ekoinfo.services.models.LoginResponseServiceModel;
import mk.finki.ekoinfo.services.models.UserServiceModel;
import mk.finki.ekoinfo.utils.JWTUtils;
import mk.finki.ekoinfo.utils.PasswordUtils;

import java.util.List;
import java.util.UUID;


public class UserService {

    private UserRepository repository;

    public UserService(){
        this.repository = new UserRepository();
    }


    /// service methods /////////

    public List<UserServiceModel> getAll(){
        return repository.getAll();
    }

    public UserServiceModel get(UUID id){
        return repository.get(id);
    }

    public UserServiceModel add(UserServiceModel user){
        // validacija -> ne smee da ima drug user so ist username
        String hashedPassword = PasswordUtils.hash(user.getPassword());
        user.setPassword(hashedPassword);
        return repository.add(user);
    }

    public UserServiceModel edit(UUID id, UserServiceModel user){
        return repository.edit(id, user);
    }

    public boolean remove(UUID id){
        return repository.remove(id);
    }


    public LoginResponseServiceModel login(CredentialsServiceModel credentials) {
        try {

            UserServiceModel user = repository.getUserByUsername(credentials.getUsername());
            boolean credentialdMatch = PasswordUtils.verify(credentials.getPassword(), user.getPassword());

            if (!credentialdMatch) throw new UnauthorizedException("Внесовте погрешни кориснички информации!");

            LoginResponseServiceModel loginResponseServiceModel = new LoginResponseServiceModel();
            loginResponseServiceModel.setToken(JWTUtils.issueTokenForUser(user));
            loginResponseServiceModel.setUser(user);
            return loginResponseServiceModel;

        } catch (UserDoesNotExist ex) {
            throw  new UnauthorizedException();
        }
    }

    public UUID extractUserIdFromToken(String token){
        try {
            UserServiceModel user = JWTUtils.verifyToken(token);
            return user.getId();
        } catch (Exception ex) {
            return null;
        }

    }

}