package mk.finki.ekoinfo.services;


import mk.finki.ekoinfo.repositories.SensorRepository;
import mk.finki.ekoinfo.services.models.SensorServiceModel;

import java.util.List;

public class SensorService {

    private SensorRepository repository;

    public SensorService(){
        this.repository = new SensorRepository();
    }


    /// service methods /////////

    public List<SensorServiceModel> getAll(){
        return repository.getAll();
    }

    public SensorServiceModel get(int id){
        return repository.get(id);
    }

    public SensorServiceModel add(SensorServiceModel sensor){
        SensorServiceModel sensorServiceModel = repository.add(sensor);
        return sensorServiceModel;
    }

    public SensorServiceModel edit(int id, SensorServiceModel sensor){
        SensorServiceModel sensorServiceModel = repository.edit(id, sensor);
        return sensorServiceModel;
    }

    public boolean remove(int id) {
        boolean sensorRemoved =  repository.remove(id);
        SensorServiceModel sensorServiceModel = new SensorServiceModel();
        sensorServiceModel.setId(id);
        return sensorRemoved;
    }
    
}
