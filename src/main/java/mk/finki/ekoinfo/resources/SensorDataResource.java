package mk.finki.ekoinfo.resources;

import mk.finki.ekoinfo.mappers.resourceToService.SensorDataRTSMapper;
import mk.finki.ekoinfo.mappers.serviceToResource.SensorDataSTRMapper;
import mk.finki.ekoinfo.resources.models.*;
import mk.finki.ekoinfo.services.SensorDataService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("sensorData")
@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class SensorDataResource {


    private SensorDataService service = new SensorDataService();

    @POST
    @Path("/ttn")
    public Response addTTNData(TTNSensorDataApiModel ttnSensorApiModel) {
        TTNSensorDataApiModel sensorData = SensorDataSTRMapper.map(service.addTTNData(SensorDataRTSMapper.map(ttnSensorApiModel)));
        return Response.status(Response.Status.CREATED).entity(sensorData).build();
    }

    @GET
    @Path("/ttn")
    public List<GetTTNDataApiModel> getTTNData() {
        return SensorDataSTRMapper.mapGetTTNData(service.getTTNData());
    }


    @POST
    @Path("/ttnFiltered")
    public List<GetTTNDataApiModel> getAllTTNDataFiltered(GetTTNDataFilteredApiModel getTTNDataFilteredApiModel) {
        return SensorDataSTRMapper.mapGetTTNData(service.getAllTTNDataFiltered(SensorDataRTSMapper.mapGetTTNDataFiltered(getTTNDataFilteredApiModel)));
    }

    @GET
    @Path("/ttnChartData")
    public List<GetChartTTNDataApiModel> getChartTTNData() {
        return SensorDataSTRMapper.mapGetChartTTNData(service.getChartTTNData());
    }

    @GET
    @Path("/ttnWineQualityModelData")
    public List<GetWineQualityDataApiModel> getWineQualityModelData() {
        return SensorDataSTRMapper.mapGetWineQualityData(service.getWineQualityModelData());
    }






    @GET
    public List<SensorDataApiModel> getAll() {
        return SensorDataSTRMapper.map(service.getAll());
    }


    @GET
    @Path("/{id}")
    public SensorDataApiModel get(@PathParam("id") int id) {
        return SensorDataSTRMapper.map(service.get(id));
    }


    @POST
    public Response add(SensorDataApiModel sensorApiModel) {
        SensorDataApiModel sensor = SensorDataSTRMapper.map(service.add(SensorDataRTSMapper.map(sensorApiModel)));
        return Response.status(Response.Status.CREATED).entity(sensor).build();
    }


    @PUT
    @Path("/{id}")
    public SensorDataApiModel edit(@PathParam("id") int id, SensorDataApiModel sensorApiModel){
        return SensorDataSTRMapper.map(service.edit(id, SensorDataRTSMapper.map(sensorApiModel)));
    }


    @DELETE
    @Path("/{id}")
    public Response remove(@PathParam("id") int id) {
        boolean deleted = service.remove(id);
        return Response.status(Response.Status.OK).build();
    }



}
