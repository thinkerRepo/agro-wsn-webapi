package mk.finki.ekoinfo.resources;

import mk.finki.ekoinfo.filters.anotations.Secured;
import mk.finki.ekoinfo.mappers.resourceToService.UserRTSMapper;
import mk.finki.ekoinfo.mappers.serviceToResource.UserSTRMapper;
import mk.finki.ekoinfo.resources.models.CredentialsApiModel;
import mk.finki.ekoinfo.resources.models.LoginResponseApiModel;
import mk.finki.ekoinfo.resources.models.UserApiModel;
import mk.finki.ekoinfo.services.UserService;

import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;


@Path("users")
@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class UserResource {

    private UserService service = new UserService();

    @GET
    public List<UserApiModel> getAll() {
        return UserSTRMapper.map(service.getAll());
    }


    @GET
    @Path("/{id}")
    public UserApiModel get(@PathParam("id") UUID id) {
        return UserSTRMapper.map(service.get(id));
    }


    @POST
    public Response add(UserApiModel userJson) {
        UserApiModel user = UserSTRMapper.map(service.add(UserRTSMapper.map(userJson)));
        return Response.status(Response.Status.CREATED).entity(user).build();
    }


    @PUT
    @Path("/{id}")
    public UserApiModel edit(@PathParam("id") UUID id, UserApiModel userJson){
        return UserSTRMapper.map(service.edit(id, UserRTSMapper.map(userJson)));
    }


    @DELETE
    @Path("/{id}")
    public Response remove(@PathParam("id") UUID id) {
        boolean deleted = service.remove(id);
        return Response.status(Response.Status.OK).build();
    }


    @POST
    @Path("/login")
    public Response login(CredentialsApiModel credentials) {
        LoginResponseApiModel loginResponse = UserSTRMapper.map(service.login(UserRTSMapper.map(credentials)));
        return Response.status(Response.Status.OK)
                .entity(loginResponse)
                .build();
    }


    @GET
    @Secured
    @Path("/testToken")
    public Response testToken(@Context ContainerRequestContext crc){
        return Response.ok(crc.getProperty("authUser")).build();
    }

}
