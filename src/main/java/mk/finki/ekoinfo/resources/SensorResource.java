package mk.finki.ekoinfo.resources;


import mk.finki.ekoinfo.mappers.resourceToService.SensorRTSMapper;
import mk.finki.ekoinfo.mappers.serviceToResource.SensorSTRMapper;
import mk.finki.ekoinfo.resources.models.SensorApiModel;
import mk.finki.ekoinfo.services.SensorService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("sensors")
@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
public class SensorResource {

    private SensorService service = new SensorService();

    @GET
    public List<SensorApiModel> getAll() {
        return SensorSTRMapper.map(service.getAll());
    }


    @GET
    @Path("/{id}")
    public SensorApiModel get(@PathParam("id") int id) {
        return SensorSTRMapper.map(service.get(id));
    }


    @POST
    public Response add(SensorApiModel sensorApiModel) {
        SensorApiModel sensor = SensorSTRMapper.map(service.add(SensorRTSMapper.map(sensorApiModel)));
        return Response.status(Response.Status.CREATED).entity(sensor).build();
    }


    @PUT
    @Path("/{id}")
    public SensorApiModel edit(@PathParam("id") int id, SensorApiModel sensorApiModel){
        return SensorSTRMapper.map(service.edit(id, SensorRTSMapper.map(sensorApiModel)));
    }


    @DELETE
    @Path("/{id}")
    public Response remove(@PathParam("id") int id) {
        boolean deleted = service.remove(id);
        return Response.status(Response.Status.OK).build();
    }


}
