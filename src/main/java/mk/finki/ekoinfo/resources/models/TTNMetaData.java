package mk.finki.ekoinfo.resources.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by srbo on 13-Mar-17.
 */
public class TTNMetaData {
    private Date time;
    private float frequency;
    private String modulation;
    private String data_rate;
    private int bit_rate;
    private String coding_rate;
    private List<TTNGateways> gateways;
    private float latitude;
    private float longitude;
    private float altitude;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public float getFrequency() {
        return frequency;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    public String getModulation() {
        return modulation;
    }

    public void setModulation(String modulation) {
        this.modulation = modulation;
    }

    public String getData_rate() {
        return data_rate;
    }

    public void setData_rate(String data_rate) {
        this.data_rate = data_rate;
    }

    public int getBit_rate() {
        return bit_rate;
    }

    public void setBit_rate(int bit_rate) {
        this.bit_rate = bit_rate;
    }

    public String getCoding_rate() {
        return coding_rate;
    }

    public void setCoding_rate(String coding_rate) {
        this.coding_rate = coding_rate;
    }

    public List<TTNGateways> getGateways() {
        return gateways;
    }

    public void setGateways(List<TTNGateways> gateways) {
        this.gateways = gateways;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }
}
