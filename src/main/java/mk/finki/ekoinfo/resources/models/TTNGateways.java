package mk.finki.ekoinfo.resources.models;

import java.util.Date;

/**
 * Created by srbo on 13-Mar-17.
 */
public class TTNGateways {
    private String gtw_id;
    private Date timestamp;
    private Date time;
    private int channel;
    private float rssi;
    private float snr;
    private int rf_chain;
    private float latitude;
    private float longitude;
    private float altitude;
}

