package mk.finki.ekoinfo.resources.models;

import mk.finki.ekoinfo.utils.DateAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GetTTNDataFilteredApiModel {

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date dateFrom;

    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date dateTo;



    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

}
