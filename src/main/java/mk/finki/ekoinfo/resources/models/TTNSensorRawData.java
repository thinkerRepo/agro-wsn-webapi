package mk.finki.ekoinfo.resources.models;

/**
 * Created by srbo on 13-Mar-17.
 */
public class TTNSensorRawData {
      private boolean batteryStatus;
      private int humidity;
      private int leaf;
      private int soil;
      private int temperature;

    public boolean isBatteryStatus() {
        return batteryStatus;
    }

    public void setBatteryStatus(boolean batteryStatus) {
        this.batteryStatus = batteryStatus;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getLeaf() {
        return leaf;
    }

    public void setLeaf(int leaf) {
        this.leaf = leaf;
    }

    public int getSoil() {
        return soil;
    }

    public void setSoil(int soil) {
        this.soil = soil;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }
}
