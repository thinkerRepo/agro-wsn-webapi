package mk.finki.ekoinfo.resources.models;

/**
 * Created by srbo on 13-Mar-17.
 */
public class TTNSensorDataApiModel {
    private String app_id;
    private String dev_id;
    private String hardware_serial;
    private int port;
    private int  counter;
    private boolean is_retry;
    private boolean confirmed;
    private String payload_raw;
    private TTNSensorRawData payload_fields;
    private TTNMetaData metadata;



    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getDev_id() {
        return dev_id;
    }

    public void setDev_id(String dev_id) {
        this.dev_id = dev_id;
    }

    public String getHardware_serial() {
        return hardware_serial;
    }

    public void setHardware_serial(String hardware_serial) {
        this.hardware_serial = hardware_serial;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public boolean isIs_retry() {
        return is_retry;
    }

    public void setIs_retry(boolean is_retry) {
        this.is_retry = is_retry;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public String getPayload_raw() {
        return payload_raw;
    }

    public void setPayload_raw(String payload_raw) {
        this.payload_raw = payload_raw;
    }

    public TTNSensorRawData getPayload_fields() {
        return payload_fields;
    }

    public void setPayload_fields(TTNSensorRawData payload_fields) {
        this.payload_fields = payload_fields;
    }

    public TTNMetaData getMetadata() {
        return metadata;
    }

    public void setMetadata(TTNMetaData metadata) {
        this.metadata = metadata;
    }
}