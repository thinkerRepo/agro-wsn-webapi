package mk.finki.ekoinfo.resources.models;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class CredentialsApiModel {

    private String username;
    private String password;

    public CredentialsApiModel() {}

    public CredentialsApiModel(String username, String password) {
        this.username = username;
        this.password = password;
    }


    /// getters & setters /////////

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
