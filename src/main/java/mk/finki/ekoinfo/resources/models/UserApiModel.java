package mk.finki.ekoinfo.resources.models;

import mk.finki.ekoinfo.utils.UUIDAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.UUID;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UserApiModel {

    @XmlJavaTypeAdapter(UUIDAdapter.class)
    private UUID id;
    private String username;
    private String password;

    public UserApiModel() {}

    public UserApiModel(UUID id, String username, String password){
        this.id = id;
        this.username = username;
        this.password = password;
    }


    /// getters & setters /////////

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

