package mk.finki.ekoinfo.resources.models;

public class GetChartTTNDataApiModel {

    private int hour;
    private int temperature;
    private int humidity;
    private int soilMoisture;
    private int leafWetness;


    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getSoilMoisture() {
        return soilMoisture;
    }

    public void setSoilMoisture(int soilMoisture) {
        this.soilMoisture = soilMoisture;
    }

    public int getLeafWetness() {
        return leafWetness;
    }

    public void setLeafWetness(int leafWetness) {
        this.leafWetness = leafWetness;
    }
}
