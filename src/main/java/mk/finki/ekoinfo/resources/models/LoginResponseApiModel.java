package mk.finki.ekoinfo.resources.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoginResponseApiModel {

    private String token;
    private UserApiModel user;

    public LoginResponseApiModel() {}

    public LoginResponseApiModel(String token, UserApiModel user) {
        this.token = token;
        this.user = user;
    }


    /// getters & setters /////////

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserApiModel getUser() {
        return user;
    }

    public void setUser(UserApiModel user) {
        this.user = user;
    }
}