package mk.finki.ekoinfo.mappers.resourceToService;

import mk.finki.ekoinfo.resources.models.GetTTNDataFilteredApiModel;
import mk.finki.ekoinfo.resources.models.SensorDataApiModel;
import mk.finki.ekoinfo.resources.models.TTNSensorDataApiModel;
import mk.finki.ekoinfo.services.models.GetTTNDataFilteredServiceModel;
import mk.finki.ekoinfo.services.models.SensorDataServiceModel;
import mk.finki.ekoinfo.services.models.TTNSensorDataServiceModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by srbo on 06-Feb-17.
 */
public class SensorDataRTSMapper {
    
    public static SensorDataServiceModel map(SensorDataApiModel sensorDataApiModel) {
        SensorDataServiceModel sensorServiceModel = new SensorDataServiceModel();

        sensorServiceModel.setId(sensorDataApiModel.getId());
        sensorServiceModel.setSensorId(sensorDataApiModel.getSensorId());
        sensorServiceModel.setDate(sensorDataApiModel.getDate());

        sensorServiceModel.setAirTemperature(sensorDataApiModel.getAirTemperature());
        sensorServiceModel.setAirHumidity(sensorDataApiModel.getAirHumidity());
        sensorServiceModel.setSoilTemperature(sensorDataApiModel.getSoilTemperature());
        sensorServiceModel.setSoilHumidity(sensorDataApiModel.getSoilHumidity());
        sensorServiceModel.setSoilPH(sensorDataApiModel.getSoilPH());
        sensorServiceModel.setLeafWetness(sensorDataApiModel.getLeafWetness());
        sensorServiceModel.setLuminosity(sensorDataApiModel.getLuminosity());

        return sensorServiceModel;
    }

    public static TTNSensorDataServiceModel map (TTNSensorDataApiModel ttnSensorDataApiModel){
        TTNSensorDataServiceModel ttnData = new TTNSensorDataServiceModel();

        ttnData.setDeviceId(ttnSensorDataApiModel.getDev_id());
        ttnData.setDate(new Date());

        ttnData.setAltitude(ttnSensorDataApiModel.getMetadata().getAltitude());
        ttnData.setLongitude(ttnSensorDataApiModel.getMetadata().getLongitude());
        ttnData.setLatitude(ttnSensorDataApiModel.getMetadata().getLatitude());

        ttnData.setBatteryStatus(ttnSensorDataApiModel.getPayload_fields().isBatteryStatus());
        ttnData.setLeafWetness(ttnSensorDataApiModel.getPayload_fields().getLeaf());
        ttnData.setSoilMoisture(ttnSensorDataApiModel.getPayload_fields().getSoil());
        ttnData.setTemperature(ttnSensorDataApiModel.getPayload_fields().getTemperature());
        ttnData.setHumidity(ttnSensorDataApiModel.getPayload_fields().getHumidity());

        return ttnData;
    }

    public static List<SensorDataServiceModel> map(List<SensorDataApiModel> sensorsDataApiModel){
        List<SensorDataServiceModel> sensorsDataService = new ArrayList<>();

        for (SensorDataApiModel sensorDataApiModel : sensorsDataApiModel) {
            sensorsDataService.add(map(sensorDataApiModel));
        }

        return sensorsDataService;
    }


    public static GetTTNDataFilteredServiceModel mapGetTTNDataFiltered(GetTTNDataFilteredApiModel entity) {
        GetTTNDataFilteredServiceModel serviceModel = new GetTTNDataFilteredServiceModel();

        serviceModel.setDateFrom(entity.getDateFrom());
        serviceModel.setDateTo(entity.getDateTo());

        return serviceModel;
    }

    
}
