package mk.finki.ekoinfo.mappers.resourceToService;


import mk.finki.ekoinfo.resources.models.SensorApiModel;
import mk.finki.ekoinfo.services.models.SensorServiceModel;

import java.util.ArrayList;
import java.util.List;

public class SensorRTSMapper {

    public static SensorServiceModel map(SensorApiModel sensorApiModel) {
        SensorServiceModel sensorServiceModel = new SensorServiceModel();

        sensorServiceModel.setId(sensorApiModel.getId());
        sensorServiceModel.setSifra(sensorApiModel.getSifra());
        sensorServiceModel.setLat(sensorApiModel.getLat());
        sensorServiceModel.setLon(sensorApiModel.getLon());

        return sensorServiceModel;
    }

    public static List<SensorServiceModel> map(List<SensorApiModel> sensoriApiModel){
        List<SensorServiceModel> sensoriService = new ArrayList<>();

        for (SensorApiModel sensorApiModel : sensoriApiModel) {
            sensoriService.add(map(sensorApiModel));
        }

        return sensoriService;
    }
}
