package mk.finki.ekoinfo.mappers.resourceToService;

import mk.finki.ekoinfo.resources.models.CredentialsApiModel;
import mk.finki.ekoinfo.resources.models.UserApiModel;
import mk.finki.ekoinfo.services.models.CredentialsServiceModel;
import mk.finki.ekoinfo.services.models.UserServiceModel;

import java.util.ArrayList;
import java.util.List;


public class UserRTSMapper {

    public static UserServiceModel map(UserApiModel userJson) {
        UserServiceModel userServiceModel = new UserServiceModel();

        userServiceModel.setId(userJson.getId());
        userServiceModel.setUsername(userJson.getUsername());
        userServiceModel.setPassword(userJson.getPassword());

        return userServiceModel;
    }

    public static List<UserServiceModel> map(List<UserApiModel> useriJson){
        List<UserServiceModel> useriService = new ArrayList<>();

        for (UserApiModel userJson : useriJson) {
            useriService.add(map(userJson));
        }

        return useriService;
    }

    public static CredentialsServiceModel map(CredentialsApiModel credentialsJson) {
        CredentialsServiceModel credentialsServiceModel = new CredentialsServiceModel();

        credentialsServiceModel.setUsername(credentialsJson.getUsername());
        credentialsServiceModel.setPassword(credentialsJson.getPassword());

        return credentialsServiceModel;
    }
}