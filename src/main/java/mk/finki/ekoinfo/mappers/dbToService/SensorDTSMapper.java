package mk.finki.ekoinfo.mappers.dbToService;


import mk.finki.ekoinfo.repositories.models.SensorDbModel;
import mk.finki.ekoinfo.services.models.SensorServiceModel;

import java.util.ArrayList;
import java.util.List;

public class SensorDTSMapper {

    public static SensorServiceModel map(SensorDbModel sensorDb) {
        SensorServiceModel sensorServiceModel = new SensorServiceModel();

        sensorServiceModel.setId(sensorDb.getId());
        sensorServiceModel.setSifra(sensorDb.getSifra());
        sensorServiceModel.setLat(sensorDb.getLat());
        sensorServiceModel.setLon(sensorDb.getLon());

        return sensorServiceModel;
    }

    public static List<SensorServiceModel> map(List<SensorDbModel> sensoriDb){
        List<SensorServiceModel> sensoriService = new ArrayList<>();

        for (SensorDbModel sensorDb : sensoriDb) {
            sensoriService.add(map(sensorDb));
        }

        return sensoriService;
    }
    
}
