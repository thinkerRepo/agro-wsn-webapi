package mk.finki.ekoinfo.mappers.dbToService;

import mk.finki.ekoinfo.repositories.models.GetChartTTNDataDbModel;
import mk.finki.ekoinfo.repositories.models.GetWineQualityDataDbModel;
import mk.finki.ekoinfo.repositories.models.SensorDataDbModel;
import mk.finki.ekoinfo.repositories.models.TTNSensorDataDbModel;
import mk.finki.ekoinfo.services.models.GetChartTTNDataServiceModel;
import mk.finki.ekoinfo.services.models.GetWineQualityDataServiceModel;
import mk.finki.ekoinfo.services.models.SensorDataServiceModel;
import mk.finki.ekoinfo.services.models.TTNSensorDataServiceModel;

import java.util.ArrayList;
import java.util.List;


public class SensorDataDTSMapper {

    public static SensorDataServiceModel map(SensorDataDbModel sensorsDataDb) {
        SensorDataServiceModel sensorsDataServiceModel = new SensorDataServiceModel();

        sensorsDataServiceModel.setId(sensorsDataDb.getId());
        sensorsDataServiceModel.setSensorId(sensorsDataDb.getSensorId());
        sensorsDataServiceModel.setDate(sensorsDataDb.getDate());

        sensorsDataServiceModel.setAirTemperature(sensorsDataDb.getAirTemperature());
        sensorsDataServiceModel.setAirHumidity(sensorsDataDb.getAirHumidity());
        sensorsDataServiceModel.setSoilTemperature(sensorsDataDb.getSoilTemperature());
        sensorsDataServiceModel.setSoilHumidity(sensorsDataDb.getSoilHumidity());
        sensorsDataServiceModel.setSoilPH(sensorsDataDb.getSoilPH());
        sensorsDataServiceModel.setLeafWetness(sensorsDataDb.getLeafWetness());
        sensorsDataServiceModel.setLuminosity(sensorsDataDb.getLuminosity());

        return sensorsDataServiceModel;
    }

    public static TTNSensorDataServiceModel map (TTNSensorDataDbModel ttnSensorDataDbModel){
        TTNSensorDataServiceModel ttnData = new TTNSensorDataServiceModel();

        ttnData.setId(ttnSensorDataDbModel.getId());
        ttnData.setDeviceId(ttnSensorDataDbModel.getDeviceId());
        ttnData.setDate(ttnSensorDataDbModel.getDate());

        ttnData.setAltitude(ttnSensorDataDbModel.getAltitude());
        ttnData.setLongitude(ttnSensorDataDbModel.getLongitude());
        ttnData.setLatitude(ttnSensorDataDbModel.getLatitude());

        ttnData.setBatteryStatus(ttnSensorDataDbModel.isBatteryStatus());
        ttnData.setLeafWetness(ttnSensorDataDbModel.getLeafWetness());
        ttnData.setSoilMoisture(ttnSensorDataDbModel.getSoilMoisture());
        ttnData.setTemperature(ttnSensorDataDbModel.getTemperature());
        ttnData.setHumidity(ttnSensorDataDbModel.getHumidity());

        return ttnData;
    }

    public static List<SensorDataServiceModel> map(List<SensorDataDbModel> sensorsDataDb){
        List<SensorDataServiceModel> sensorsDataService = new ArrayList<>();

        for (SensorDataDbModel sensorDataDb : sensorsDataDb) {
            sensorsDataService.add(map(sensorDataDb));
        }

        return sensorsDataService;
    }

    public static List<TTNSensorDataServiceModel> mapTTNData(List<TTNSensorDataDbModel> sensorsDataDb){
        List<TTNSensorDataServiceModel> sensorsDataService = new ArrayList<>();

        for (TTNSensorDataDbModel sensorDataDb : sensorsDataDb) {
            sensorsDataService.add(map(sensorDataDb));
        }

        return sensorsDataService;
    }


    public static List<GetChartTTNDataServiceModel> mapGetChartTTNData(List<GetChartTTNDataDbModel> chartTtnSensorDataServiceModels){
        List<GetChartTTNDataServiceModel> chartDataApiModels = new ArrayList<>();

        for (GetChartTTNDataDbModel entity : chartTtnSensorDataServiceModels) {
            chartDataApiModels.add(mapGetChartTTNData(entity));
        }

        return chartDataApiModels;
    }

    public static GetChartTTNDataServiceModel mapGetChartTTNData(GetChartTTNDataDbModel entity){
        GetChartTTNDataServiceModel resultEntity = new GetChartTTNDataServiceModel();

        resultEntity.setHour(entity.getHour());
        resultEntity.setTemperature(entity.getTemperature());
        resultEntity.setHumidity(entity.getHumidity());
        resultEntity.setSoilMoisture(entity.getSoilMoisture());
        resultEntity.setLeafWetness(entity.getLeafWetness());

        return resultEntity;
    }

    public static List<GetWineQualityDataServiceModel> mapGetWineQualityData(List<GetWineQualityDataDbModel> dbEntities){
        List<GetWineQualityDataServiceModel> serviceEntities = new ArrayList<>();

        for (GetWineQualityDataDbModel dbEntity : dbEntities) {
            serviceEntities.add(mapGetWineQualityData(dbEntity));
        }

        return serviceEntities;
    }

    public static GetWineQualityDataServiceModel mapGetWineQualityData(GetWineQualityDataDbModel entity){
        GetWineQualityDataServiceModel resultEntity = new GetWineQualityDataServiceModel();

        resultEntity.setDate(entity.getDate());
        resultEntity.setTemperature(entity.getTemperature());

        return resultEntity;
    }

}
