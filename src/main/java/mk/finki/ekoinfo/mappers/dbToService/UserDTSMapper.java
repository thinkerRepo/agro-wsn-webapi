package mk.finki.ekoinfo.mappers.dbToService;

import mk.finki.ekoinfo.repositories.models.UserDbModel;
import mk.finki.ekoinfo.services.models.UserServiceModel;

import java.util.ArrayList;
import java.util.List;

public class UserDTSMapper {

    public static UserServiceModel map(UserDbModel userDb) {
        UserServiceModel userServiceModel = new UserServiceModel();

        userServiceModel.setId(userDb.getId());
        userServiceModel.setUsername(userDb.getUsername());
        userServiceModel.setPassword(userDb.getPassword());

        return userServiceModel;
    }

    public static List<UserServiceModel> map(List<UserDbModel> useriDb){
        List<UserServiceModel> useriService = new ArrayList<>();

        for (UserDbModel userDb : useriDb) {
            useriService.add(map(userDb));
        }

        return useriService;
    }
}