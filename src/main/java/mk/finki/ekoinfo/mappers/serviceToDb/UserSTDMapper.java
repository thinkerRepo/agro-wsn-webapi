package mk.finki.ekoinfo.mappers.serviceToDb;

import mk.finki.ekoinfo.repositories.models.UserDbModel;
import mk.finki.ekoinfo.services.models.UserServiceModel;

import java.util.ArrayList;
import java.util.List;

public class UserSTDMapper {

    public static UserDbModel map(UserServiceModel userService) {
        UserDbModel userDb = new UserDbModel();

        userDb.setId(userService.getId());
        userDb.setUsername(userService.getUsername());
        userDb.setPassword(userService.getPassword());

        return userDb;
    }

    public static List<UserDbModel> map(List<UserServiceModel> useriService){
        List<UserDbModel> useriDb = new ArrayList<>();

        for (UserServiceModel userService : useriService) {
            useriDb.add(map(userService));
        }

        return useriDb;
    }



}