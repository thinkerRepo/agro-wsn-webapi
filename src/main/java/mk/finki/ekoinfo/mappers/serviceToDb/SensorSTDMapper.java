package mk.finki.ekoinfo.mappers.serviceToDb;


import mk.finki.ekoinfo.repositories.models.SensorDbModel;
import mk.finki.ekoinfo.services.models.SensorServiceModel;

import java.util.ArrayList;
import java.util.List;

public class SensorSTDMapper {

    public static SensorDbModel map(SensorServiceModel sensorService) {
        SensorDbModel sensorDb = new SensorDbModel();

        sensorDb.setId(sensorService.getId());
        sensorDb.setSifra(sensorService.getSifra());
        sensorDb.setLat(sensorService.getLat());
        sensorDb.setLon(sensorService.getLon());

        return sensorDb;
    }

    public static List<SensorDbModel> map(List<SensorServiceModel> sensoriService){
        List<SensorDbModel> sensoriDb = new ArrayList<>();

        for (SensorServiceModel sensorService : sensoriService) {
            sensoriDb.add(map(sensorService));
        }

        return sensoriDb;
    }
}
