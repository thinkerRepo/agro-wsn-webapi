package mk.finki.ekoinfo.mappers.serviceToDb;

import mk.finki.ekoinfo.repositories.models.GetTTNDataFilteredDbModel;
import mk.finki.ekoinfo.repositories.models.SensorDataDbModel;
import mk.finki.ekoinfo.repositories.models.TTNSensorDataDbModel;
import mk.finki.ekoinfo.services.models.GetTTNDataFilteredServiceModel;
import mk.finki.ekoinfo.services.models.SensorDataServiceModel;
import mk.finki.ekoinfo.services.models.TTNSensorDataServiceModel;

import java.util.ArrayList;
import java.util.List;


public class SensorDataSTDMapper {

    public static SensorDataDbModel map(SensorDataServiceModel sensorDataService) {
        SensorDataDbModel sensorDb = new SensorDataDbModel();

        sensorDb.setId(sensorDataService.getId());
        sensorDb.setSensorId(sensorDataService.getSensorId());
        sensorDb.setDate(sensorDataService.getDate());

        sensorDb.setAirTemperature(sensorDataService.getAirTemperature());
        sensorDb.setAirHumidity(sensorDataService.getAirHumidity());
        sensorDb.setSoilTemperature(sensorDataService.getSoilTemperature());
        sensorDb.setSoilHumidity(sensorDataService.getSoilHumidity());
        sensorDb.setSoilPH(sensorDataService.getSoilPH());
        sensorDb.setLeafWetness(sensorDataService.getLeafWetness());
        sensorDb.setLuminosity(sensorDataService.getLuminosity());

        return sensorDb;
    }

    public static TTNSensorDataDbModel map (TTNSensorDataServiceModel ttnSensorDataServiceModel){
        TTNSensorDataDbModel ttnData = new TTNSensorDataDbModel();

        ttnData.setId(ttnSensorDataServiceModel.getId());
        ttnData.setDeviceId(ttnSensorDataServiceModel.getDeviceId());
        ttnData.setDate(ttnSensorDataServiceModel.getDate());

        ttnData.setAltitude(ttnSensorDataServiceModel.getAltitude());
        ttnData.setLongitude(ttnSensorDataServiceModel.getLongitude());
        ttnData.setLatitude(ttnSensorDataServiceModel.getLatitude());

        ttnData.setBatteryStatus(ttnSensorDataServiceModel.isBatteryStatus());
        ttnData.setLeafWetness(ttnSensorDataServiceModel.getLeafWetness());
        ttnData.setSoilMoisture(ttnSensorDataServiceModel.getSoilMoisture());
        ttnData.setTemperature(ttnSensorDataServiceModel.getTemperature());
        ttnData.setHumidity(ttnSensorDataServiceModel.getHumidity());

        return ttnData;
    }

    public static List<SensorDataDbModel> map(List<SensorDataServiceModel> sensorsDataService){
        List<SensorDataDbModel> sensorsDataDb = new ArrayList<>();

        for (SensorDataServiceModel sensorDataService : sensorsDataService) {
            sensorsDataDb.add(map(sensorDataService));
        }

        return sensorsDataDb;
    }

    public static GetTTNDataFilteredDbModel mapGetTTNDataFiltered(GetTTNDataFilteredServiceModel entity) {
        GetTTNDataFilteredDbModel dbModel = new GetTTNDataFilteredDbModel();

        dbModel.setDateFrom(entity.getDateFrom());
        dbModel.setDateTo(entity.getDateTo());

        return dbModel;
    }

}
