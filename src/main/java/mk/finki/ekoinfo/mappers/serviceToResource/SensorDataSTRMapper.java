package mk.finki.ekoinfo.mappers.serviceToResource;

import mk.finki.ekoinfo.resources.models.*;
import mk.finki.ekoinfo.services.models.GetChartTTNDataServiceModel;
import mk.finki.ekoinfo.services.models.GetWineQualityDataServiceModel;
import mk.finki.ekoinfo.services.models.SensorDataServiceModel;
import mk.finki.ekoinfo.services.models.TTNSensorDataServiceModel;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class SensorDataSTRMapper {

    public static SensorDataApiModel map(SensorDataServiceModel sensorDataService) {
        SensorDataApiModel sensorApiModel = new SensorDataApiModel();

        sensorApiModel.setId(sensorDataService.getId());
        sensorApiModel.setSensorId(sensorDataService.getSensorId());
        sensorApiModel.setDate(sensorDataService.getDate());

        sensorApiModel.setAirTemperature(sensorDataService.getAirTemperature());
        sensorApiModel.setAirHumidity(sensorDataService.getAirHumidity());
        sensorApiModel.setSoilTemperature(sensorDataService.getSoilTemperature());
        sensorApiModel.setSoilHumidity(sensorDataService.getSoilHumidity());
        sensorApiModel.setSoilPH(sensorDataService.getSoilPH());
        sensorApiModel.setLeafWetness(sensorDataService.getLeafWetness());
        sensorApiModel.setLuminosity(sensorDataService.getLuminosity());

        return sensorApiModel;
    }

    public static TTNSensorDataApiModel map (TTNSensorDataServiceModel ttnSensorDataServiceModel){
        TTNSensorDataApiModel ttnData = new TTNSensorDataApiModel();

        TTNSensorRawData payloadFields = new TTNSensorRawData();
        TTNMetaData metadata = new TTNMetaData();

        ttnData.setDev_id(ttnSensorDataServiceModel.getDeviceId());

        metadata.setAltitude(ttnSensorDataServiceModel.getAltitude());
        metadata.setLongitude(ttnSensorDataServiceModel.getLongitude());
        metadata.setLatitude(ttnSensorDataServiceModel.getLatitude());

        payloadFields.setBatteryStatus(ttnSensorDataServiceModel.isBatteryStatus());
        payloadFields.setLeaf(ttnSensorDataServiceModel.getLeafWetness());
        payloadFields.setSoil(ttnSensorDataServiceModel.getSoilMoisture());
        payloadFields.setTemperature(ttnSensorDataServiceModel.getTemperature());
        payloadFields.setHumidity(ttnSensorDataServiceModel.getHumidity());

        ttnData.setPayload_fields(payloadFields);
        ttnData.setMetadata(metadata);

        return ttnData;
    }

    public static GetTTNDataApiModel mapGetTTNData(TTNSensorDataServiceModel ttnSensorDataServiceModel){
        GetTTNDataApiModel ttnData = new GetTTNDataApiModel();

        ttnData.setDeviceId(ttnSensorDataServiceModel.getDeviceId());
        ttnData.setDate(ttnSensorDataServiceModel.getDate());

        ttnData.setAltitude(ttnSensorDataServiceModel.getAltitude());
        ttnData.setLongitude(ttnSensorDataServiceModel.getLongitude());
        ttnData.setLatitude(ttnSensorDataServiceModel.getLatitude());

        ttnData.setBatteryStatus(ttnSensorDataServiceModel.isBatteryStatus());
        ttnData.setLeafWetness(ttnSensorDataServiceModel.getLeafWetness());
        ttnData.setSoilMoisture(ttnSensorDataServiceModel.getSoilMoisture());
        ttnData.setTemperature(ttnSensorDataServiceModel.getTemperature());
        ttnData.setHumidity(ttnSensorDataServiceModel.getHumidity());

        return ttnData;
    }

    public static List<SensorDataApiModel> map(List<SensorDataServiceModel> sensorsDataService){
        List<SensorDataApiModel> sensorsDataApiModel = new ArrayList<>();

        for (SensorDataServiceModel sensorDataService : sensorsDataService) {
            sensorsDataApiModel.add(map(sensorDataService));
        }

        return sensorsDataApiModel;
    }


    public static List<GetTTNDataApiModel> mapGetTTNData(List<TTNSensorDataServiceModel> ttnSensorDataServiceModels){
        List<GetTTNDataApiModel> getTTNDataApiModels = new ArrayList<>();

        for (TTNSensorDataServiceModel entity : ttnSensorDataServiceModels) {
            getTTNDataApiModels.add(mapGetTTNData(entity));
        }

        return getTTNDataApiModels;
    }



    public static List<GetChartTTNDataApiModel> mapGetChartTTNData(List<GetChartTTNDataServiceModel> chartTtnSensorDataServiceModels){
        List<GetChartTTNDataApiModel> chartDataApiModels = new ArrayList<>();

        for (GetChartTTNDataServiceModel entity : chartTtnSensorDataServiceModels) {
            chartDataApiModels.add(mapGetChartTTNData(entity));
        }

        return chartDataApiModels;
    }

    public static GetChartTTNDataApiModel mapGetChartTTNData(GetChartTTNDataServiceModel entity){
        GetChartTTNDataApiModel resultEntity = new GetChartTTNDataApiModel();

        resultEntity.setHour(entity.getHour());
        resultEntity.setTemperature(entity.getTemperature());
        resultEntity.setHumidity(entity.getHumidity());
        resultEntity.setSoilMoisture(entity.getSoilMoisture());
        resultEntity.setLeafWetness(entity.getLeafWetness());

        return resultEntity;
    }

    public static List<GetWineQualityDataApiModel> mapGetWineQualityData(List<GetWineQualityDataServiceModel> serviceEntities){
        List<GetWineQualityDataApiModel> apiEntities = new ArrayList<>();

        for (GetWineQualityDataServiceModel serviceEntity : serviceEntities) {
            apiEntities.add(mapGetWineQualityData(serviceEntity));
        }

        return apiEntities;
    }

    public static GetWineQualityDataApiModel mapGetWineQualityData(GetWineQualityDataServiceModel entity){
        GetWineQualityDataApiModel resultEntity = new GetWineQualityDataApiModel();

        resultEntity.setDate(entity.getDate());
        resultEntity.setTemperature(entity.getTemperature());

        return resultEntity;
    }


}
