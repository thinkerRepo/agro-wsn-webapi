package mk.finki.ekoinfo.mappers.serviceToResource;


import mk.finki.ekoinfo.resources.models.SensorApiModel;
import mk.finki.ekoinfo.services.models.SensorServiceModel;

import java.util.ArrayList;
import java.util.List;

public class SensorSTRMapper {

    public static SensorApiModel map(SensorServiceModel sensorService) {
        SensorApiModel sensorApiModel = new SensorApiModel();

        sensorApiModel.setId(sensorService.getId());
        sensorApiModel.setSifra(sensorService.getSifra());
        sensorApiModel.setLat(sensorService.getLat());
        sensorApiModel.setLon(sensorService.getLon());

        return sensorApiModel;
    }

    public static List<SensorApiModel> map(List<SensorServiceModel> sensoriService){
        List<SensorApiModel> sensoriApiModel = new ArrayList<>();

        for (SensorServiceModel sensorService : sensoriService) {
            sensoriApiModel.add(map(sensorService));
        }

        return sensoriApiModel;
    }
    
    
}
