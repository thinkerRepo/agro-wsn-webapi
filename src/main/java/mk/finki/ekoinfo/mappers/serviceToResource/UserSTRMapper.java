package mk.finki.ekoinfo.mappers.serviceToResource;

import mk.finki.ekoinfo.resources.models.LoginResponseApiModel;
import mk.finki.ekoinfo.resources.models.UserApiModel;
import mk.finki.ekoinfo.services.models.LoginResponseServiceModel;
import mk.finki.ekoinfo.services.models.UserServiceModel;

import java.util.ArrayList;
import java.util.List;

public class UserSTRMapper {

    public static UserApiModel map(UserServiceModel userService) {
        UserApiModel userJson = new UserApiModel();

        userJson.setId(userService.getId());
        userJson.setUsername(userService.getUsername());
        userJson.setPassword(userService.getPassword());

        return userJson;
    }

    public static List<UserApiModel> map(List<UserServiceModel> useriService){
        List<UserApiModel> useriJson = new ArrayList<>();

        for (UserServiceModel userService : useriService) {
            useriJson.add(map(userService));
        }

        return useriJson;
    }

    public static LoginResponseApiModel map(LoginResponseServiceModel loginResponseServiceModel){
        LoginResponseApiModel loginResponseJson = new LoginResponseApiModel();

        loginResponseJson.setToken(loginResponseServiceModel.getToken());
        loginResponseJson.setUser(map(loginResponseServiceModel.getUser()));

        return loginResponseJson;
    }
}
