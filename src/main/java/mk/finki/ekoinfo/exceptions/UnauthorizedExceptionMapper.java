package mk.finki.ekoinfo.exceptions;

import mk.finki.ekoinfo.exceptions.models.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UnauthorizedExceptionMapper implements ExceptionMapper<UnauthorizedException> {

    @Override
    public Response toResponse(UnauthorizedException ex) {
        ErrorMessage errorMessage = new ErrorMessage(ex.getMessage(), 401, "http://docs.busline.misw.finki.ukim.mk/unauthorizedException");
        return Response.status(Response.Status.UNAUTHORIZED)
                .entity(errorMessage)
                .build();
    }
}
