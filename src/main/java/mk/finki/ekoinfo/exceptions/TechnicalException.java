package mk.finki.ekoinfo.exceptions;


public class TechnicalException extends RuntimeException {

    public TechnicalException(String message) {
        super(message);
    }
}
