package mk.finki.ekoinfo.exceptions;

public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException() {
        super("Не сте авторизирани! Ве молиме најавете се!");
    }

    public UnauthorizedException(String message) {
        super(message);
    }
}