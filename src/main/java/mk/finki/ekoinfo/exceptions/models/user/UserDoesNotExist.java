package mk.finki.ekoinfo.exceptions.models.user;

import mk.finki.ekoinfo.exceptions.DataNotFoundException;


public class UserDoesNotExist extends DataNotFoundException {

    public UserDoesNotExist(){
        super("Бараниот корисник не е пронајден!");
    }

    public UserDoesNotExist(String message){
        super(message);
    }
}
