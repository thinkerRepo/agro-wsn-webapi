package mk.finki.ekoinfo.utils;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import mk.finki.ekoinfo.exceptions.UnauthorizedException;
import mk.finki.ekoinfo.exceptions.models.user.UserDoesNotExist;
import mk.finki.ekoinfo.services.UserService;
import mk.finki.ekoinfo.services.models.UserServiceModel;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


public class JWTUtils {

    private static final String issuer = "http://ekoinfo.finki.ukim.mk";
    private static final String secret = "secret#1";
    private static final UserService userService = new UserService();

    public static String issueTokenForUser(UserServiceModel user) {

        final long iat = System.currentTimeMillis() / 1000l; // issued at claim
        final long exp = iat + 28800L; // expires claim. In this case the token expires in 28800 seconds = 8h

        final JWTSigner signer = new JWTSigner(secret);
        final HashMap<String, Object> claims = new HashMap<>();
        claims.put("iss", issuer);
        claims.put("sub", user.getId().toString());
        claims.put("exp", exp);
        claims.put("iat", iat);

        return signer.sign(claims);
    }

    public static UserServiceModel verifyToken(String jwt){

        try {

            JWTVerifier jwtVerifier = new JWTVerifier(secret);
            Map<String,Object> claims= jwtVerifier.verify(jwt);
            String userId = (String) claims.get("sub");
            if(userId == null) throw new JWTVerifyException();
            return userService.get(UUID.fromString(userId));

        } catch (JWTVerifyException e) {
            throw new UnauthorizedException("Вашиот токен е не валиден!");
        } catch (UserDoesNotExist ex) {
            throw new UnauthorizedException("Корисникот асоциран со вашиот токен, не постои!");
        } catch (Exception e) {
            throw new RuntimeException("Настана проблем при верификација на токенот!");
        }
    }

}
