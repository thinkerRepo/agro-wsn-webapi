package mk.finki.ekoinfo.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {

    private static final String username = "postgres";
    private static final String password = "Masterkey#1";
    private static final String dbUrl = "Masterkey#1";


    public static Connection getConnection() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/agrowsn?user=" + username + "&password=" + password;
        return DriverManager.getConnection(url);
    }

}
