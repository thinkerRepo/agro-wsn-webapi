package mk.finki.ekoinfo.utils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.UUID;

public class UUIDAdapter extends XmlAdapter<String, UUID> {

    @Override
    public UUID unmarshal(String v) throws Exception {
        return v != null ? UUID.fromString(v) : null;
    }

    @Override
    public String marshal(UUID v) throws Exception {
        return v != null ?  v.toString() : null;
    }
}
