(function () {
    "use strict";

    agGrid.initialiseAgGridWithAngular1(angular);

    var module = angular.module("app", [
        "ngComponentRouter",
        "ngStorage",
        "ngSanitize",
        "angularComponentModalService",
        "ngMaterial",
        "ngMessages",
        "agGrid",
        "chart.js",

        "app",
        "app.components",
        "app.backend"

    ]);

    module.config(config);
    module.run(run);

    module.value("$routerRootComponent", "agrowsnApp");



    config.$inject = ["$mdThemingProvider"];

    function config($mdThemingProvider) {

        $mdThemingProvider.theme('default')
            .primaryPalette('indigo')
            .accentPalette('red');

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }



    run.$inject = [];

    function run() {

    }

})();