(function () {
    
    "use strict";

    var module = angular.module("app.backend", []);

    module.config(config);
    module.run(run);

    config.$inject = [];

    function config() {
        
    }

    run.$inject = [];

    function run() {
        
    }
    
})();