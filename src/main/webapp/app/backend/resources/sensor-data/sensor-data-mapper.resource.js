(function () {

    'use strict';

    angular
        .module('app.backend')
        .factory('sensorDataMapper', sensorDataMapper);

    sensorDataMapper.$inject = [];

    function sensorDataMapper() {

        return {
            mapGet : mapGet,
            mapPost : mapPost,
            mapPostGetTTNDataFiltered: mapPostGetTTNDataFiltered,
            mapGetChartData: mapGetChartData,
            mapGetWineQualityModelData: mapGetWineQualityModelData
        };


        /// interface implementation /////////

        function mapGet(entity) {
            return {
                id: entity.id,
                date: entity.date,
                deviceId: entity.deviceId,
                latitude: entity.latitude,
                longitude: entity.longitude,
                altitude: entity.altitude,
                batteryStatus: entity.batteryStatus,
                humidity: entity.humidity,
                leafWetness: entity.leafWetness,
                soilMoisture: entity.soilMoisture,
                temperature: entity.temperature
            };
        }

        function mapPost(entity) {
            return {

            };
        }

        function mapPostGetTTNDataFiltered(entity) {
            return {
                dateFrom: entity.dateFrom,
                dateTo: entity.dateTo
            }
        }

        function mapGetChartData(entity) {
            return {
                hour: entity.hour,
                temperature: entity.temperature,
                humidity: entity.humidity,
                soilMoisture: entity.soilMoisture,
                leafWetness: entity.leafWetness
            }
        }


        function mapGetWineQualityModelData(entity) {
            return {
                date: entity.date,
                temperature: entity.temperature
            }
        }


    }

})();