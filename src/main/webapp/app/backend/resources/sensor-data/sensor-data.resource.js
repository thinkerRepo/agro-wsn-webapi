(function (){
    "use strict";

    var module = angular.module("app.backend");

    module.service("sensorDataResource", sensorDataResource);

    sensorDataResource.$inject = [
        "$http",
        "$q",
        "urlsConfig",
        "utils",
        "sensorDataMapper",
        "Toastr"
    ];

    function sensorDataResource(
        $http,
        $q,
        urlsConfig,
        utils,
        sensorDataMapper
    ) {


        var baseUrl = urlsConfig.getApiURI();
        var resourceId = "sensorData";


        this.getAllTTNDataFiltered = getAllTTNDataFiltered;
        this.getChartTTNData = getChartTTNData;
        this.getWineQualityModelData = getWineQualityModelData;






        /// backend functions /////////


        function getAllTTNDataFiltered(filters, config) {
            var deferred = $q.defer();

            var url = baseUrl + resourceId + "/ttnFiltered";
            var content = sensorDataMapper.mapPostGetTTNDataFiltered(filters);

            $http.post(url, content, config).then(function (response) {
                var mappedData = utils.linq.select(response.data, sensorDataMapper.mapGet);
                deferred.resolve(mappedData);
            }, function (error) {
                Toastr.showObject({
                    message: "Error while retrieving sensor data!",
                    title: "Error",
                    theme: "error"
                });
                deferred.reject(error);
            });

            return deferred.promise;
        }



        function getChartTTNData(config) {
            var deferred = $q.defer();

            var url = baseUrl + resourceId + "/ttnChartData";

            $http.get(url, config).then(function (response) {
                var mappedData = utils.linq.select(response.data, sensorDataMapper.mapGetChartData);
                deferred.resolve(mappedData);
            }, function (error) {
                Toastr.showObject({
                    message: "Error while retrieving chart sensor data!",
                    title: "Error",
                    theme: "error"
                });
                deferred.reject(error);
            });

            return deferred.promise;
        }



        function getWineQualityModelData(config) {
            var deferred = $q.defer();

            var url = baseUrl + resourceId + "/ttnWineQualityModelData";

            $http.get(url, config).then(function (response) {
                var mappedData = utils.linq.select(response.data, sensorDataMapper.mapGetWineQualityModelData);
                deferred.resolve(mappedData);
            }, function (error) {
                Toastr.showObject({
                    message: "Error while retrieving wine quality model data!",
                    title: "Error",
                    theme: "error"
                });
                deferred.reject(error);
            });

            return deferred.promise;
        }

        // mock
        /*function getWineQualityModelData(config) {
            var deferred = $q.defer();

            var mappedData = [
                {date: "28.06.2017", temperature: 29},
                {date: "29.06.2017", temperature: 30},
                {date: "30.06.2017", temperature: 31},
                {date: "01.07.2017", temperature: 32},
                {date: "02.07.2017", temperature: 27},
                {date: "03.07.2017", temperature: 22},
                {date: "04.07.2017", temperature: 24},
                {date: "05.07.2017", temperature: 27},
                {date: "06.07.2017", temperature: 26},
                {date: "07.07.2017", temperature: 30}
            ];

            deferred.resolve(mappedData);

            return deferred.promise;
        }*/




    }

})();