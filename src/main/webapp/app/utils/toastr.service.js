(function () {
    "use strict";

    var module = angular.module("app");
    module.service("Toastr", Toastr);

    Toastr.$inject = [];

    function Toastr() {

        this.show = show;
        this.showObject = showObject;


        /// implementation /////////

        function show(message, title, theme, delay) {

            if(message !== null && typeof message !== "string" && typeof message === "object") return showObject(message);

            delay = delay || 3000;
            theme = theme || "success";

            if(theme === "success") toastr.success(message, title, {timeOut: delay});
            else if(theme === "error") toastr.error(message, title, {timeOut: delay});
            else if(theme === "warning") toastr.warning(message, title, {timeOut: delay});

        }

        function showObject(toastrObject) {

            var delay = toastrObject.delay || 3000;
            var theme = toastrObject.theme || "success";
            var message = toastrObject.message || "";
            var title = toastrObject.title || "";

            if(theme === "success") toastr.success(message, title, {timeOut: delay});
            else if(theme === "error") toastr.error(message, title, {timeOut: delay});
            else if(theme === "warning") toastr.warning(message, title, {timeOut: delay});

        }

    }

})();