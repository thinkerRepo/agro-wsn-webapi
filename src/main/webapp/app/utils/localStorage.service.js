/**
 * Created by srbo on 24-Jun-17.
 */

(function () {
    "use strict";

    var module = angular.module("app");
    module.service("LocalStorage", LocalStorage);

    LocalStorage.$inject = ["$localStorage"];

    function LocalStorage($localStorage) {
        this.put = put;
        this.get = get;

        /// implementation /////////

        function put(key, value) {
            if(!$localStorage.ecashStorage) {
                $localStorage.ecashStorage = {};
            }

            $localStorage.ecashStorage[key] = {
                value: value
            };
        }

        function get(key) {
            if($localStorage.ecashStorage) {
                var cachedInstance = $localStorage.ecashStorage[key];
                if(cachedInstance) return cachedInstance.value;
            }
        }
    }


})();