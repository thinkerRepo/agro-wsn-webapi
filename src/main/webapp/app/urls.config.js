/**
 * Created by srbo on 28-Jun-17.
 */

(function () {
    "use strict";

    var module = angular.module("app");
    module.factory("urlsConfig", urlsConfig);


    function urlsConfig() {

        //var url = "http://localhost:8080/api/sensorData/ttn";
        //var url = "http://188.166.74.13:8080/agro-wsn-webapi/api/sensorData/ttn";

        var host = "http://localhost:8080/";

        return {
            getApiURI: getApiURI,
            getSocketURI: getSocketURI
        };


        function getApiURI() {
            return getHostName(host) + "api/";
        }

        function getSocketURI() {
            return getHostName(host) + "signalr";
        }

        function getHostName(host) {
            return host.charAt(host.length - 1) === "/" ?  host : host + "/";
        }
    }


})();