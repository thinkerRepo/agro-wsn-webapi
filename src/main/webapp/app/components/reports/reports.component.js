(function (){
    "use strict";

    var module = angular.module("app.components");

    module.component("reports", {
        templateUrl: "app/components/reports/reports.component.html",
        controllerAs: "vm",
        controller: controller,
        bindings: {

        }
    });

    controller.$inject = ["sensorDataResource"];

    function controller(sensorDataResource) {

        var vm = this;

        this.$onInit = function () {
            vm.dateFrom = moment("2017-06-01", "YYYY-MM-DD");
            vm.dateTo = moment("2017-06-30", "YYYY-MM-DD");

            initializeGrid();
            getSensorData();
        };

        this.$onDestroy = function () {

        };



        vm.getSensorData = getSensorData;



        /// implementation /////////

        function getSensorData() {
            sensorDataResource.getAllTTNDataFiltered({ dateFrom: vm.dateFrom, dateTo: vm.dateTo }).then(function (data) {
                updateGridRows(data);
            }).catch(function (reason) {

            });
        }






        // grid

        function initializeGrid() {
            var columnDefs = [
                {headerName: "Device ID", field: "deviceId"},
                {headerName: "Date", field: "date"},
                {headerName: "Temperature", field: "temperature"},
                {headerName: "Humidity", field: "humidity"},
                {headerName: "Soil Moisture", field: "soilMoisture"},
                {headerName: "Leaf Wetness", field: "leafWetness"},
                {headerName: "Battery Status", field: "batteryStatus"}
            ];


            vm.gridOptions = {
                columnDefs: columnDefs,
                rowData: [],
                enableColResize: true,
                enableSorting: true,
                enableFilter: true,
                onGridReady: function (params) {
                    params.api.sizeColumnsToFit();
                }
            };

        }


        function updateGridRows(data) {
            vm.gridOptions.api.setRowData(data);
        }




    }


})();