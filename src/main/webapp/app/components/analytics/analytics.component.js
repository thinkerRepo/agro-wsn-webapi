(function (){
    "use strict";

    var module = angular.module("app.components");

    module.component("analytics", {
        templateUrl: "app/components/analytics/analytics.component.html",
        controllerAs: "vm",
        controller: controller,
        bindings: {

        }
    });

    controller.$inject = ["utils", "sensorDataResource"];

    function controller(utils, sensorDataResource) {

        var vm = this;

        this.$onInit = function () {
            initializeGrid();

            sensorDataResource.getWineQualityModelData().then(function (data) {
                buildTemperatureChart(data);
                updateGridRows(data);
            }).catch(function (reason) {

            });
        };



        function buildTemperatureChart(data) {

            var dateLabels = utils.linq.select(data, function (x) {
                return x.date;
            });

            var temperatureData = utils.linq.select(data, function (x) {
                return x.temperature;
            });

            vm.temperatureChart = {
                labels: dateLabels,
                series: ["Temperature"],
                colors: ["#00ADF9"],
                options: {
                    responsive: true,
                    maintainAspectRatio: false
                },
                data: temperatureData,
                onClick: function (points, evt) {
                    console.log(points, evt);
                }
            };
        }



        // grid

        function initializeGrid() {
            var columnDefs = [
                {
                    headerName: "Date",
                    field: "date"
                },
                {
                    headerName: "Temperature",
                    field: "temperature",
                    cellStyle: function(params) {
                        if (params.value >= 13 &&  params.value <= 21) return { backgroundColor: 'green' };
                        else if ((params.value >= -2 &&  params.value <= 13) || (params.value >= 21 &&  params.value <= 35)) return { backgroundColor: 'orange' };
                        else return { backgroundColor: 'red' };
                    }
                }
            ];


            vm.gridOptions = {
                columnDefs: columnDefs,
                rowData: [],
                enableColResize: true,
                enableSorting: true,
                enableFilter: true,
                onGridReady: function (params) {
                    params.api.sizeColumnsToFit();
                }
            };

        }


        function updateGridRows(data) {
            vm.gridOptions.api.setRowData(data);
        }




    }
})();