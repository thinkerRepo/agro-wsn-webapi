(function (){
    "use strict";

    var module = angular.module("app.components");

    module.component("dashboard", {
        templateUrl: "app/components/dashboard/dashboard.component.html",
        controllerAs: "vm",
        controller: controller,
        bindings: {

        }
    });

    controller.$inject = ["$scope", "sensorDataResource", "utils"];

    function controller($scope, sensorDataResource, utils) {

        var vm = this;

        this.$onInit = function () {

            var hourLabels = [
                "00:00h",
                "01:00h",
                "02:00h",
                "03:00h",
                "04:00h",
                "05:00h",
                "06:00h",
                "07:00h",
                "08:00h",
                "09:00h",
                "10:00h",
                "11:00h",
                "12:00h",
                "13:00h",
                "14:00h",
                "15:00h",
                "16:00h",
                "17:00h",
                "18:00h",
                "19:00h",
                "20:00h",
                "21:00h",
                "22:00h",
                "23:00h"
            ];

            vm.temperatureChart = {
                labels: hourLabels,
                series: ["Temperature"],
                colors: ["#99d066"],
                options: {
                    responsive: true,
                    maintainAspectRatio: false
                },
                data: [],
                onClick: function (points, evt) {
                    console.log(points, evt);
                }
            };

            vm.humidityChart = {
                labels: hourLabels,
                series: ["Humidity"],
                colors: ["#00ADF9"],
                options: {
                    responsive: true,
                    maintainAspectRatio: false
                },
                data: [],
                onClick: function (points, evt) {
                    console.log(points, evt);
                }
            };


            vm.soilMoistureChart = {
                labels: hourLabels,
                series: ["Soil Moisture"],
                colors: ["#FDB45C"],
                options: {
                    responsive: true,
                    maintainAspectRatio: false
                },
                data: [],
                onClick: function (points, evt) {
                    console.log(points, evt);
                }
            };

            vm.leafWetnessChart = {
                labels: hourLabels,
                series: ["Leaf Wetness"],
                colors: ["#4D5360"],
                options: {
                    responsive: true,
                    maintainAspectRatio: false
                },
                data: [],
                onClick: function (points, evt) {
                    console.log(points, evt);
                }
            };


            sensorDataResource.getChartTTNData().then(function (chartData) {
                loadChartData(chartData);
            }).catch(function (reason) {

            });

        };




        function loadChartData(chartData) {
            vm.temperatureChart.data = utils.linq.select(chartData, function (entity) {
                return entity.temperature;
            });

            vm.humidityChart.data = utils.linq.select(chartData, function (entity) {
                return entity.humidity;
            });

            vm.soilMoistureChart.data = utils.linq.select(chartData, function (entity) {
                return entity.soilMoisture;
            });

            vm.leafWetnessChart.data = utils.linq.select(chartData, function (entity) {
                return entity.leafWetness;
            });
        }






/*
        new Chart(document.getElementById("temperature-chart"), {
            type: 'line',
            data: {
                labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
                datasets: [{
                    data: [86,114,106,106,107,111,133,221,783,2478],
                    label: "Africa",
                    borderColor: "#3e95cd",
                    fill: false
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: false,
                    text: 'World population per region (in millions)'
                }
            }
        });

        new Chart(document.getElementById("humidity-chart"), {
            type: 'line',
            data: {
                labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
                datasets: [{
                    data: [86,114,106,106,107,111,133,221,783,2478],
                    label: "Africa",
                    borderColor: "#3e95cd",
                    fill: false
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: false,
                    text: 'World population per region (in millions)'
                }
            }
        });

        new Chart(document.getElementById("soil-moisture-chart"), {
            type: 'line',
            data: {
                labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
                datasets: [{
                    data: [86,114,106,106,107,111,133,221,783,2478],
                    label: "Africa",
                    borderColor: "#3e95cd",
                    fill: false
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: false,
                    text: 'World population per region (in millions)'
                }
            }
        });

        new Chart(document.getElementById("leaf-wetness-chart"), {
            type: 'line',
            data: {
                labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
                datasets: [{
                    data: [86,114,106,106,107,111,133,221,783,2478],
                    label: "Africa",
                    borderColor: "#3e95cd",
                    fill: false
                }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: false,
                    text: 'World population per region (in millions)'
                }
            }
        });
*/

    }


})();