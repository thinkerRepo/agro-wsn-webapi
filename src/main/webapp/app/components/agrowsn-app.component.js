(function (){
    "use strict";

    var module = angular.module("app.components");

    module.component("agrowsnApp", {
        templateUrl: "app/components/agrowsn-app.component.html",
        controllerAs: "vm",
        controller: controller,
        $routeConfig: [
            { path: "/dashboard", component: "dashboard", name: "Dashboard"},
            { path: "/reports", component: "reports", name: "Reports"},
            { path: "/analytics", component: "analytics", name: "Analytics"},
            { path: "/**", redirectTo: ["Dashboard"]}
        ]
    });

    controller.$inject = ["$mdSidenav"];

    function controller($mdSidenav) {

        var vm = this;

        vm.toggleSideNav = toggleSideNav;



        /// implementation /////////

        function toggleSideNav() {
            $mdSidenav("left").toggle();
        }

    }


})();